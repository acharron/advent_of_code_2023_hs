import qualified Data.Text as T
import qualified Lib as L

type RGB = (Int,Int,Int)

main = do
    input <- getContents
    let linesList = lines input  -- Get the "aaa\nbbbb\ncccc\ndddd" input and returns ["aaa","bbbb","cccc","dddd"]
        res1 = doTheMagic1 linesList
        res2 = doTheMagic2 linesList
--    print [gameIdAndStatus s | s <- linesList]
    print ("Part 1: " ++ show res1)
    print ("Part 2: " ++ show res2)


doTheMagic1 :: [String] -> Int
doTheMagic1 ss = sum validGamesIds
  where
    allGames = [gameIdAndStatus s | s <- ss]
    validGamesIds = [a | (a,b) <- allGames, b]

doTheMagic2 :: [String] -> Int
doTheMagic2 ss = sum powers
  where
    gameDrawsStr = [T.splitOn (T.pack ": ") (T.pack s) !! 1 | s <- ss]  -- ["3 blue ...", "5 red ...", ...]
    rgbRequireds = [maxRGB (parseCountOneGame g) | g <- gameDrawsStr]
    powers = [r*g*b | (r,g,b) <- rgbRequireds]


-- "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
-- > (1,True)
gameIdAndStatus :: String -> (Int,Bool)
gameIdAndStatus s = (gameId, state)
  where
    parts = T.splitOn (T.pack ": ") (T.pack s)
    gameId = L.readInt . reverse . takeWhile L.isDigit . reverse . T.unpack . head $ parts
    gameResult = parts !! 1
    state = isValidGame (maxRGB (parseCountOneGame gameResult))


isValidGame :: RGB -> Bool
isValidGame (r,g,b) = r <= 12 && g <= 13 && b <= 14

maxRGB :: [RGB] -> RGB
maxRGB [] = (0,0,0)
maxRGB [(r,g,b)] = (r,g,b)
maxRGB ((r,g,b):(r',g',b'):others) = maxRGB ((max r r', max g g', max b b') : others)

-- "3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
-- > [(4,0,3), (1,2,6), (0,2,0)]
parseCountOneGame :: T.Text -> [RGB]
parseCountOneGame t = map parseCountOneGrab (T.splitOn (T.pack "; ") t)

-- "3 blue, 4 red"
-- > (4,0,3)
parseCountOneGrab :: T.Text -> RGB
parseCountOneGrab t = maxRGBWithStr (0,0,0) parts
  where
    parts = map T.unpack (T.splitOn (T.pack ", ") t)


maxRGBWithStr :: RGB -> [String] -> RGB
maxRGBWithStr (r,g,b) [] = (r,g,b)
maxRGBWithStr (r,g,b) (s:other_s)
  | L.endsWith "red" s   = maxRGBWithStr (max r (val s), g, b) other_s
  | L.endsWith "green" s = maxRGBWithStr (r, max g (val s), b) other_s
  | L.endsWith "blue" s  = maxRGBWithStr (r, g, max b (val s)) other_s
  | otherwise = maxRGBWithStr (r,g,b) other_s
--  where
--    val :: String -> Int
--    val = read . takeWhile L.isDigit

val :: String -> Int
val "" = 0
val s = read (takeWhile L.isDigit s)
