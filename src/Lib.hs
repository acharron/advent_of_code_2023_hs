module Lib where


someFunc :: IO ()
someFunc = print "aaaa"


readInt = (read :: String -> Int)


isDigit :: Char -> Bool
isDigit c = c `elem` ['0'..'9']


beginsWith :: Eq a => [a] -> [a] -> Bool
beginsWith [] _ = True
beginsWith _ [] = False
beginsWith (x:xs) (y:ys) = (x == y) && beginsWith xs ys


endsWith :: Eq a => [a] -> [a] -> Bool
endsWith x y = beginsWith (reverse x) (reverse y)
