import qualified Data.Text as T
import qualified Lib as L

main = do   
    input <- getContents
    let linesList = lines input  -- Get the "aaa\nbbbb\ncccc\ndddd" input and returns ["aaa","bbbb","cccc","dddd"]
        res1 = doTheMagic1 linesList
        res2 = doTheMagic2 linesList
    print ("Part 1: " ++ show res1)
    print ("Part 2: " ++ show res2)



firstDigit :: String -> String
firstDigit "" = ""
firstDigit (x:xs)
    | L.isDigit x = [x]
    | otherwise = firstDigit xs


lastDigit :: String -> String
lastDigit "" = ""
lastDigit x
    | L.isDigit lastChar = [lastChar]
    | otherwise = lastDigit (init x)
    where lastChar = last x


getFirstLastDigitsAsInt :: String -> Int
getFirstLastDigitsAsInt "" = error "Empty str"
getFirstLastDigitsAsInt s
    | not (null firstStr) && not (null lastStr) = read (firstStr ++ lastStr) :: Int
    | otherwise = 0
    where firstStr = firstDigit s
          lastStr = lastDigit s


replaceNumbers :: T.Text -> T.Text
replaceNumbers t = t_res
    where t1 = pupReplace "one" "one1one" t
          t2 = pupReplace "two" "two2two" t1
          t3 = pupReplace "three" "three3three" t2
          t4 = pupReplace "four" "four4four" t3
          t5 = pupReplace "five" "five5five" t4
          t6 = pupReplace "six" "six6six" t5
          t7 = pupReplace "seven" "seven7seven" t6
          t8 = pupReplace "eight" "eight8eight" t7
          t_res = pupReplace "nine" "nine9nine" t8


pupReplace :: String -> String -> T.Text -> T.Text
pupReplace s1 s2 = T.replace (T.pack s1) (T.pack s2)


pupReplaceNumbers :: String -> String
pupReplaceNumbers s = T.unpack (replaceNumbers (T.pack s))



doTheMagic1 :: [String] -> Int
doTheMagic1 xs = sum [getFirstLastDigitsAsInt x | x <- xs]

doTheMagic2 :: [String] -> Int
doTheMagic2 xs = doTheMagic1 fixed_xs
    where fixed_xs = [pupReplaceNumbers x | x <- xs]


