import qualified Data.Text as T
import qualified Lib as L

type Row = Int
type Col = Int
type Coord = (Row, Col)
type Box = (Coord, Coord)  -- top/left - bottom/right corners
type Board = [String]



main = do
    input <- getContents
    let linesList = lines input  -- Get the "aaa\nbbbb\ncccc\ndddd" input and returns ["aaa","bbbb","cccc","dddd"]
        res1 = doTheMagic1 linesList
--        res2 = doTheMagic2 linesList
    print ("Part 1: " ++ show res1)
--    print ("Part 2: " ++ show res2)


--doTheMagic1 :: [String] -> Char
doTheMagic1 ss = ss !! 0 !! 2



{-
We have a grid in the [String] input lineList
   0  1  2  3  4  5  6  7  ...
0  4  6  7  .  .  1  1  4
1  .  .  .  *  .  .  .  .
2  .  .  3  5  .  .  6  3
3  .  .  .  .  .  .  #  .
...

linesList !! 0  = "467..114.."
linesList !! 1  = "...*......"
linesList !! 0 !! 2 = '7'
-}

-- If I take a line, I need to find all the integers on that line, and their col indices
-- Result should be (Int, [Col])