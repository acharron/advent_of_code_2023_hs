
{-
Useful functions

## Lists
How to make a list : "Append to the head of an empty list"
    3:[]  ->  [3]
    4:2:6:3:[]    ->  [4,2,6,3]
    [4,3]:[]      ->  [[4,3]]  (It's a list of list)
    [4,3] ++ [2]  ->  [4,3,2]  (That's how to concat multiple lists)
    5:[3,5,6]     ->  [5,3,5,6]  (To append at the head one thing, just use : )

head  tail  last  init
length   null   reverse
take
    take 3 [1,2,3,4,5] -> [1,2,3]
drop
    drop 3 [1,2,3,4,5] -> [4,5]
maximum   minimum  sum  product
elem
    "is element of list?"
cycle
    cycle [1,2,3] -> [1,2,3,1,2,3,1,2,3,1,2,3,1,..........]
    cycle "LOL " -> "LOL LOL LOL LOL LOL LOL LOL ........."
repeat
    repeat 5 -> [5,5,5,5,5,5,5,5,5,......]
replicate
    replicate 3 5 -> [5,5,5]

## Tuples/Pairs
fst
    fst (8, "haha") -> 8          ! Doesn't work on bigger tuples
snd
    snd (8, "haha") -> "haha"     ! Doesn't work on bigger tuples

zip
    zip [1,2,3] ["a", "b", "c"] -> [(1,"a"), (2,"b"), (3,"c")
    zip [1..] ["a", "b", "c", "d"] -> Same

## High order

zipWith
    zipWith (+) [4,2,5,6] [2,6,2,3]  ->  [6,8,7,9]
    zipWith max [6,3,2,7] [3,7,4,1]  ->  [6,7,4,7]
    zipWith (*) (replicate 5 2) [1..]  ->  [2,4,6,8,10]

map
    map (+3) [1,5,3,1,6]  ->  [4,8,6,4,9]
    Same as [x+3 | x <- [1,5,3,1,6]], but map needs less bracket and is more readable for those case

filter
    filter (>3) [1,5,3,2,1,6,4,3,2,1]  ->  [5,6,4]
    filter even [1..10]  ->  [2,4,6,8,10]
    Same as [x | x <- [1..10], even x]

takeWhile
    Arguments are a predicate and a list
    Fetch the elements in the list while the predicate holds true. At the first False, it stops
    let f (a,b) = snd (a,b) < 500
    takeWhile f [(n,n^2) | n <- [1..], odd (n^2)]   -- All odd squares until 500 (and the start number)
      -> [(1,1),(3,9),(5,25),(7,49),(9,81),(11,121),(13,169),(15,225),(17,289),(19,361),(21,441)]
    Same rewritten with a lambda
    takeWhile (\(a,b) -> snd (a,b) < 500) [(n,n^2) | n <- [1..], odd (n^2)]

## Types / Typeclasses

read  (Like eval in JS/Python, parse the string and use the contents as a value)
    read "5" - 2  ->  3
    read "True"  :: Bool



## I/O

putStrLn  putStr  putChar   print
getChar
sequence
mapM
    mapM print [1,2,3] ->  1  2  3  [(),(),()]
mapM_
    mapM_ print [1,2,3] ->  1  2  3
getContents
interact
    main = interact myFunctionAAA
    is equivalent to main = do  a <- getContents  putStr(myFunctionAAA a)



-}
import Control.Monad  



doubleMe x = x + x

doubleUs x y = doubleMe x + doubleMe y

doubleSmallNumber x = if x > 100
                        then x
                        else doubleMe x

fetchFromList l n = l !! n

headNullSafe list = if null list
              then -1
              else head list


head' :: [p] -> p
head' [] = error "Can't do that"
head' list = head list


factorial :: Integral a => a -> a
factorial 0 = 1
factorial n = n * factorial (n-1)



length' [] = 0
length' (_:x) = 1 + length' x



bmiTell :: (RealFloat a) => a -> String  
bmiTell 0 = "Oh oh.."
bmiTell bmi  
    | bmi <= 18.5 = "You're underweight, you emo, you!"  
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  



max' :: (Ord a) => a -> a -> a
max' a b
    | a < b = b
    | otherwise = a


initialsName :: String -> String -> String  
initialsName firstname lastname = [f] ++ ". " ++ [l] ++ "."  
    where (f:_) = firstname  
          (l:_) = lastname    



maximum' :: Ord a => [a] -> a
maximum' [] = error "Empty"
maximum' [x] = x
maximum' (x:xs) = max x (maximum' xs)



replicate' :: Int -> a -> [a]
replicate' n x
    | n <= 0 = []
    | otherwise = x:replicate' (n-1) x


take' :: Int -> [a] -> [a]
take' n _
    | n <= 0 = []
take' _ [] = []
take' n (x:xs) = x : take' (n-1) xs




zip' :: [a] -> [b] -> [(a,b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys 



quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =  smallerSorted ++ [x] ++ biggerSorted
    where smallerSorted = quicksort [a | a <- xs, a <= x]
          biggerSorted = quicksort [a | a <- xs, a > x]


largestDivisible :: Integral a => a -> a
largestDivisible a = head (filter p [100000,99999..])
  where p x = x `mod` a == 0


collatzChain :: (Integral a) => a -> [a]
collatzChain 1 = [1]
collatzChain n
    | n < 1 = n : []
    | even n = n : collatzChain (div n 2)
    | odd n = n : collatzChain (n * 3 + 1)
    | otherwise = n : []

chainsLonger15 = length [n | n <- [1,2..100], length (collatzChain n) > 15]


listOfMultFunctions = map (*) [0..]  -- We get a list of functions "multiply by index"
-- (listOfMultFunctions !! 4)  is a function that says "multiply by 4"


main1 = do   
    colors <- forM [1..4] (\a -> do  
        putStrLn $ "Which color do you associate with the number " ++ show a ++ "?"  
        getLine)
    putStrLn "The colors that you associate with 1, 2, 3 and 4 are: "  
    mapM putStrLn colors  


main2 :: IO ()
main2 = interact shortLinesOnly  
  
  
shortLinesOnly :: String -> String  
shortLinesOnly input =   
    let allLines = lines input  
        shortLines = filter (\line -> length line < 10) allLines  
        result = unlines shortLines  
    in  result  


reverseWords :: String -> String
reverseWords "" = ""
reverseWords s = unwords (map reverse (words s))





